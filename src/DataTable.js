import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Table } from 'semantic-ui-react'
const DataTable = () => {
  const url = process.env.REACT_APP_API
  const [users, setUsers] = useState([])
  useEffect(() => {
    axios.get(url).then((res) => setUsers(res.data))
  }, [])
  return (
    <Table striped>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Name</Table.HeaderCell>
          <Table.HeaderCell>Age</Table.HeaderCell>
          <Table.HeaderCell>Salary</Table.HeaderCell>
          <Table.HeaderCell>Hobby</Table.HeaderCell>
        </Table.Row>
      </Table.Header>

      <Table.Body>
        {users.map((u) => (
          <Table.Row key={u.name + u.salary}>
            <Table.Cell>{u.name}</Table.Cell>
            <Table.Cell>{u.age}</Table.Cell>
            <Table.Cell>{u.salary}</Table.Cell>
            <Table.Cell>{u.hobby}</Table.Cell>
          </Table.Row>
        ))}
      </Table.Body>
    </Table>
  )
}

export default DataTable
