import React, { useState, useEffect } from 'react'
import { Button, Form, Container, Header } from 'semantic-ui-react'
import './Formulaire.css'
import axios from 'axios'
const Formulaire = () => {
  const [user, setUser] = useState({ name: '', age: '', salary: '', hobby: '' })
  const url = process.env.REACT_APP_API
  const [valid, setValid] = useState(true)
  useEffect(() => {
    validate()
  }, [user])
  const changeHandler = (e) => {
    setUser({ ...user, [e.target.name]: e.target.value })
  }

  const submitForm = (e) => {
    e.preventDefault()
    axios.post(url, user).then((response) => {
      console.log(response)
    })
  }

  const validate = () => {
    if (!user.age || !user.name || !user.salary || !user.hobby) setValid(true)
    else setValid(false)
  }
  return (
    <Container fluid className='container'>
      <Header as='h2'>React Google Sheets!</Header>
      <Form className='form'>
        <Form.Field>
          <label>Name</label>
          <input
            name='name'
            placeholder='Enter your name'
            onChange={changeHandler}
          />
        </Form.Field>
        <Form.Field>
          <label>Age</label>
          <input
            name='age'
            type='number'
            placeholder='Enter your age'
            onChange={changeHandler}
          />
        </Form.Field>
        <Form.Field>
          <label>Salary</label>
          <input
            name='salary'
            type='number'
            onChange={changeHandler}
            placeholder='Enter your salary'
          />
        </Form.Field>
        <Form.Field>
          <label>Hobby</label>
          <input
            name='hobby'
            onChange={changeHandler}
            placeholder='Enter your hobby'
          />
        </Form.Field>

        <Button
          color='blue'
          disabled={valid}
          onClick={submitForm}
          type='submit'
        >
          Submit
        </Button>
      </Form>
    </Container>
  )
}

export default Formulaire
