import './App.css'
import DataTable from './DataTable'
import Formulaire from './Formulaire'
import { Grid, Container, Segment } from 'semantic-ui-react'
function App() {
  return (
    <Container>
      <Grid columns='equal'>
        <Grid.Row>
          <Grid.Column width={5}>
            <Segment>
              <Formulaire />
            </Segment>
          </Grid.Column>
          <Grid.Column width={10}>
            <Segment>
              {' '}
              <DataTable />
            </Segment>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  )
}

export default App
